inherit autotools-brokensep pkgconfig

DESCRIPTION = "Minijail is a sandboxing and containment tool used in Chrome OS and Android. It provides an executable that can be used to launch and sandbox other programs, and a library that can be used by code to sandbox itself."
HOMEPAGE = "https://github.com/google/minijail"

LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/${LICENSE};md5=550794465ba0ec5312d6919e203a55f9"

SRC_URI = "${CLO_LA_GIT}/platform/external/minijail.git;protocol=https;rev=c30d299f93f94a4ee4b786d15e1fb194f8709b9f;nobranch=1"
SRC_URI[sha256sum] = "1ee5a5916491a32c121c7422b4d8c16481c0396a3acab34bf1c44589dcf810ae"

FILESPATH =+ "${WORKSPACE}/:"

DEPENDS = "libcap"

S = "${WORKDIR}/git"

FILES:${PN}-dev = "${includedir}"
FILES:${PN} += "${base_libdir} ${base_bindir}"

do_install() {
    mkdir -p ${D}/${base_libdir}
    install -m 0644 libminijail.so ${D}/${base_libdir}/libminijail.so
    install -m 0644 libminijailpreload.so ${D}/${base_libdir}/libminijailpreload.so

    mkdir -p ${D}/${includedir}
    install -m 0644 libminijail.h ${D}/${includedir}/libminijail.h

    mkdir -p ${D}/${base_bindir}
    install -m 0755 minijail0 ${D}/${base_bindir}/minijail0
}
